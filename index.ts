import 'es6-shim'

import * as LineIn from 'line-in'
import { Observable } from 'rxjs/Rx'
import * as PD from 'probability-distributions'

import { valoresUnicos } from './valores-unicos'

import { QuadradoDoMeio } from './quadrado-do-meio'
import { CongruenteLinearPotencia } from './congruente-linear-potencia'
import { CongruenteLinearAdaptado } from './congruente-linear-adaptado'
import { CongruenteLinearMultiplicativo } from './congruente-linear-multiplicativo'
import { AtrasoDeFibonacci } from './atraso-de-fibonacci'
import { GeradorNumerosAleatorios } from "./gerador-numeros-aleatorios";
import { MeuMetodo } from "./meu-metodo";

function gerarAmostra(gerador: GeradorNumerosAleatorios, quantidade: number): number[] {
    const resultado: number[] = []
    for (let i = 0; i < quantidade; i++) {
        resultado.push(gerador.gerarProximo())
    }
    return resultado
}

function verificarRepeticoes(amostra: number[]): { [key: number]: number } {
    const repeticoes: { [key: number]: number } = {}
    for (const item of amostra) {
        if (repeticoes[item] === undefined) {
            repeticoes[item] = 1
            continue
        }
        repeticoes[item] += 1
    }
    return repeticoes
}

function qui2(
    amostra: number[],
    criarDistribuicao: (amostra: number[]) => (number) => number
): number {
    return 0
}

function quiQuadrado(
    amostra: number[],
    criarDistribuicao: (amostra: number[]) => (number) => number
): number {
    const distribuicao = criarDistribuicao(amostra)
    const quantidadeItens = valoresUnicos(amostra).length
    const repeticoes = verificarRepeticoes(amostra)
    const esperado = 1 / quantidadeItens
    const encontrado: number[] = []
    for (const chave in repeticoes) {
        encontrado.push(repeticoes[chave] / quantidadeItens)
    }

    let soma = 0
    for (const observado of encontrado) {
        soma += Math.pow(observado - esperado, 2) / esperado
    }
    return soma
}

function criarQuadradoDoMeio(semente: number): GeradorNumerosAleatorios {
    return new QuadradoDoMeio(semente)
}

function criarCongruenteLinearPotencia(semente: number): GeradorNumerosAleatorios {
    return new CongruenteLinearPotencia(7, 100, semente)
}

function criarCongruenteLinearAdaptado(semente: number): GeradorNumerosAleatorios {
    return new CongruenteLinearAdaptado(7, 13, 100, semente)
}

function criarCongruenteLinearMultiplicativo(semente: number): GeradorNumerosAleatorios {
    return new CongruenteLinearMultiplicativo(7, 100, semente)
}

function criarAtrasoDeFibonacci(semente: number): GeradorNumerosAleatorios {
    const semente1 = gerarSemente1(semente)
    return new AtrasoDeFibonacci(semente, semente1, (a, b) => a % b)
}

function criarMeuMetodo(semente: number): GeradorNumerosAleatorios {
    return new MeuMetodo(100, semente)
}

function gerarSemente1(semente: number): number {
    return Math.floor(semente * 1.3)
}

function gerarSemente(): Observable<number> {
    const input = new LineIn()
    return Observable.fromEvent(input, 'data')
        .map(data => data[0])
        .take(1)
}

// [
//     // criarQuadradoDoMeio,
//     criarCongruenteLinearPotencia,
//     // criarCongruenteLinearAdaptado,
//     // criarCongruenteLinearMultiplicativo,
//     // criarAtrasoDeFibonacci,
//     // criarMeuMetodo
// ]
//     .forEach(criarMetodo => {
//         // gerarSemente().subscribe(semente => {
//         // [0, 0, 0].map(() => Math.random() * 100).forEach(semente => {
//         [500, 1500, 3000].forEach(quantidade => {
//             [0, 33, 36].forEach(semente => {
//                 const gerador = criarMetodo(semente)
//                 const amostra = gerarAmostra(gerador, quantidade)
//                 const x2 = quiQuadrado(amostra)
//                 const metodo = Object.getPrototypeOf(gerador).constructor.name
//                 let mensagem = `${metodo}, n = ${quantidade}, s = ${semente.toFixed(2)}`
//                 if (metodo === 'AtrasoDeFibonacci') {
//                     const semente1 = gerarSemente1(semente)
//                     mensagem = `${mensagem}, s1 = ${semente1.toFixed(2)}`
//                 }
//                 const xQuadrado = x2.toFixed(2)
//                 mensagem = `${mensagem}, x2 = ${xQuadrado}`
//                 console.log(mensagem)
//             })
//         })
//     });

// [criarQuadradoDoMeio].forEach(criarMetodo => {
//     [36.48/*, 9.26, 72.87*/].forEach(semente => {
//         [500/*, 1500, 3000*/].forEach(quantidade => {
//             const gerador = criarMetodo(semente)
//             const amostra = gerarAmostra(gerador, quantidade)
//             const repeticoes = verificarRepeticoes(amostra)
//             console.log(repeticoes)
//         })
//     })
// })


// const amostra = PD.rnorm(500, 7)
// const x2 = quiQuadrado(amostra)
// console.log(x2)

import { fatorial } from './helpers/fatorial'

console.log(fatorial(10))