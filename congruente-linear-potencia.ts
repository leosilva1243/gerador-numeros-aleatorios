import { GeradorNumerosAleatorios } from './gerador-numeros-aleatorios'

export class CongruenteLinearPotencia implements GeradorNumerosAleatorios {

    constructor(
        private a: number,
        private m: number,
        private n: number
    ) { }

    gerarProximo(): number {
        this.n += 1
        return Math.pow(this.a, this.n) % this.m
    }
}