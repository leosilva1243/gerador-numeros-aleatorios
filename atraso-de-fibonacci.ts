import { GeradorNumerosAleatorios } from './gerador-numeros-aleatorios'

export class AtrasoDeFibonacci implements GeradorNumerosAleatorios {
    constructor(
        private x0: number,
        private x1: number,
        private operacao: (a: number, b: number) => number
    ) { }

    gerarProximo(): number {
        const valor = this.operacao(this.x0, this.x1)
        this.x0 = this.x1
        this.x1 = valor
        return valor
    }
}