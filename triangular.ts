import { GeradorNumerosAleatorios } from './gerador-numeros-aleatorios'

export class Triangular implements GeradorNumerosAleatorios {

    constructor(
        private readonly c: number
    ) { }

    gerarProximo() {
        var u = Math.random();
        if (u < this.c) return Math.sqrt(u * this.c);
        return 1 - Math.sqrt((1 - u) * (1 - this.c));
    }
}