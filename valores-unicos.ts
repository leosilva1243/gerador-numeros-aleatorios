
export function valoresUnicos(amostra: number[]): number[] {
    const observado: { [key: number]: boolean } = {}

    for (const valor of amostra) {
        if (!observado[valor]) {
            observado[valor] = true
        }
    }

    return Object.keys(observado).map(x => Number(x))
}