import { GeradorNumerosAleatorios } from './gerador-numeros-aleatorios'

export class CongruenteLinear implements GeradorNumerosAleatorios {
    private n = 0
    private x: number | null = null

    constructor(
        private a: number,
        private b: number,
        private m: number
    ) { }

    gerarProximo(): number {
        this.n += 1
        if (this.x === null) {
            return Math.pow(this.a, this.n) % this.m
        }
        return (this.a * this.x + this.b) % this.m
    }
}