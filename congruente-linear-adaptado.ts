import { GeradorNumerosAleatorios } from './gerador-numeros-aleatorios'

export class CongruenteLinearAdaptado implements GeradorNumerosAleatorios {

    constructor(
        private a: number,
        private b: number,
        private m: number,
        private x: number
    ) { }

    gerarProximo(): number {
        this.x = (this.a * this.x + this.b) % this.m
        return this.x
    }
}