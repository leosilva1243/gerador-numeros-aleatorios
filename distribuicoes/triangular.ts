
export function criarTriangular(
    amostra: number[]
): (number) => number {
    const a = menorValor(amostra)
    const b = maiorValor(amostra)
    const c = valorMaisFrequente(amostra)
    return (x: number): number => {
        if (x < c) {
            return (2 * (x - a)) / ((b - a) * (c - a))
        }
        if (x > c) {
            return (2 * (b - x)) / ((b - a) * (b - c))
        }
        return 2 * (b - a)
    }
}

function menorValor(valores: number[]): number {
    return valores.reduce((menor, valor) => valor < menor ? valor : menor, valores[0])
}

function maiorValor(valores: number[]): number {
    return valores.reduce((maior, valor) => valor > maior ? valor : maior, valores[0])
}

function valorMaisFrequente(valores: number[]): number {
    const repeticoes: { [key: number]: number } = {}
    valores.forEach(valor => {
        repeticoes[valor] = (repeticoes[valor] || 0) + 1
    })
    return valores.sort((a, b) => a - b).shift()!
}