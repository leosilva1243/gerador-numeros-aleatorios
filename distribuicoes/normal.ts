
export function criarNormal(
    amostra: number[]
): (number) => number {
    const µ = media(amostra)
    const ø = desvioPadrao(amostra)
    return (x: number): number => {
        return (1 / (ø * Math.sqrt(2 * Math.PI)))
            * Math.exp((-1 / 2) * Math.pow(((x - µ) / ø), 2))
    }
}

function media(valores: number[]): number {
    const soma = valores.reduce((s, x) => s + x, 0)
    return soma / valores.length
}

function desvioPadrao(valores: number[]): number {
    var µ = media(valores);

    var difs2 = valores.map((valor) => {
        var dif = valor - µ;
        var dif2 = dif * dif;
        return dif2;
    });

    var variancia = media(difs2);

    return Math.sqrt(variancia);
}