import * as _ from 'lodash'

import { fatorial } from '../helpers/fatorial'
import { somatorio } from '../helpers/somatorio'

export function criarPoisson(
    amostra: number[]
): (number) => number {
    const n = amostra.length
    return (x: number): number => {
        const λ = curva(x, amostra)
        return (n * Math.pow(Math.E, -λ) * Math.pow(λ, x)) / fatorial(x)
    }
}

function curva(x: number, observado: number[]): number {
    return somatorio(observado, (a) => x * a) / somatorio(observado)
}

