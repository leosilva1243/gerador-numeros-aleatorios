
export function criarUniforme(amostra: number[]): (number) => number {
    const n = amostra.length
    return (x: number): number => {
        return x / n
    }
}