
export function memoize(fn: (fn: (number) => number, number) => number): (number) => number {
    const memo: { [key: number]: number } = {}
    let _fn: (number) => number
    _fn = (x: number): number => {
        let valor = memo[x]
        if (valor !== undefined) {
            return valor
        }
        valor = fn(_fn, x)
        memo[x] = valor
        return valor
    }
    return _fn
}