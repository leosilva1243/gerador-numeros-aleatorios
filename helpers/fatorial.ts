import { memoize } from './memoize'

export const fatorial = memoize((fn, x) => {
    if (x < 2) { return 1; }
    return x * fn(x - 1)
})