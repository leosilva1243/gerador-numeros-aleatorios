
export function somatorio(valores: number[])
export function somatorio(valores: number[], fn: (number) => number)
export function somatorio(
    valores: number[], fn?: (number) => number
): number {
    const _fn = fn || ((x) => x)
    return valores.reduce((s, x) => s + _fn(x), 0)
}