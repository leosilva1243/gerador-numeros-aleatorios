import { GeradorNumerosAleatorios } from './gerador-numeros-aleatorios'

export class CongruenteLinearMultiplicativo implements GeradorNumerosAleatorios {

    constructor(
        private a: number,
        private m: number,
        private x: number
    ) { }

    gerarProximo(): number {
        this.x = (this.a * this.x) % this.m
        return this.x
    }
}