import * as PD from 'probability-distributions'

import { GeradorNumerosAleatorios } from './gerador-numeros-aleatorios'

export class Poisson implements GeradorNumerosAleatorios {

    constructor(
        private readonly variancia: number
    ) { }

    gerarProximo(): number {
        return PD.rpois(1, this.variancia)
    }
}