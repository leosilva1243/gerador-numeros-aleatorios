import { GeradorNumerosAleatorios } from './gerador-numeros-aleatorios'
import { CongruenteLinearMultiplicativo } from "./congruente-linear-multiplicativo";

export class MeuMetodo implements GeradorNumerosAleatorios {
    private readonly gerador: CongruenteLinearMultiplicativo
    private saco: number[] = []

    constructor(
        private readonly m: number,
        semente: number
    ) {
        this.gerador = new CongruenteLinearMultiplicativo(77, m, semente)
    }

    gerarProximo(): number {
        if (this.saco.length === 0) {
            const novoSaco: number[] = []
            for (let i = 1; i <= this.m; i++) {
                novoSaco.push(i)
            }
            this.saco = novoSaco
        }
        const indice = Math.round(this.gerador.gerarProximo() / this.m * this.saco.length)
        const valor = this.saco[indice]
        this.saco.splice(indice, 1)
        return valor
    }
}